def vs():
    pred = input("Введите предложение: ")
    lowers = 0
    uppers = 0

    for i in pred:
        if i.islower():
            lowers += 1
        elif i.isupper():
            uppers += 1
    sum = lowers + uppers

    a =  lowers * 100 / sum
    b = uppers * 100 / sum
    return "маленькие " + str(round(a)) + "%,\nбольшие " + str(round(b)) + "%"
print(vs())
